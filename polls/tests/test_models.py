import datetime

from django.core.urlresolvers import reverse
from django.utils import timezone
from django.test import TestCase

from ..models import Question

import pytest


@pytest.mark.parametrize("test_time,expected", [
    (timezone.now() + datetime.timedelta(days=30), False),
    (timezone.now() - datetime.timedelta(days=30), False),
    (timezone.now() - datetime.timedelta(hours=1), True),
])
def test_was_published_recently(test_time, expected):
    print("time for current test: {}".format(test_time))
    question = Question(pub_date=test_time)
    assert question.was_published_recently() is expected
